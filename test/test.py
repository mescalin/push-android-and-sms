#!/usr/bin/env python
#coding=utf-8

import json
import random
import string
import unittest
import urllib2

class TestPush(unittest.TestCase):
    def setUp(self):
        import os, sys
        sys.path.append(os.getcwd() + '/getui')

    # def testPushApp(self):
    #     d = {
    #         'appname': 'test',
    #         'title': 'to_app_title',
    #         'text': ''.join(random.sample(string.letters, 20)),
    #         'trans_content': 'app_tc',
    #         'logo': 'icon.png',
    #     }
    #     print(d)
    #     import getui.push
    #     getui.push.pushMessageToApp(d)

    # def testPushSingle(self):
    #     d = {
    #         'appname': 'test',
    #         'title': 'to_single_title',
    #         'text': ''.join(random.sample(string.letters, 20)),
    #         'trans_content': 'single_tc',
    #         'logo': 'icon.png',
    #         'cid': ['3d0451a77ec82de9b8e9f32485299ae6'],
    #     }
    #     print(d)
    #     import getui.push
    #     getui.push.pushMessageToSingle(d)

    # def testPushList(self):
    #     d = {
    #         'appname': 'test',
    #         'title': 'to_list_title',
    #         'text': ''.join(random.sample(string.letters, 20)),
    #         'trans_content': 'list_tc',
    #         'logo': 'icon.png',
    #         'cid': ['3d0451a77ec82de9b8e9f32485299ae6'],
    #     }
    #     print(d)
    #     import getui.push
    #     getui.push.pushMessageToList(d)

    def testPush(self):
        d = {
            'platform': 'android',
            'appname': 'test',
            'title': 'to_app_title',
            'text': ''.join(random.sample(string.letters, 20)),
            'trans_content': {
                '透传内容': '这是什么',
                '测试键': '测试值',
             },
            'logo': 'icon.png',
            'ring': True,
            'vibrate': True,
            'cid': [],
        }

        req = urllib2.Request('http://127.0.0.1:9002/api/ldj/push', headers = {'Content-Type': 'application/json'})
        data = json.dumps(d, ensure_ascii = False)
        print('app getui: ', data)
        print urllib2.urlopen(req, data).read()

        req = urllib2.Request('http://127.0.0.1:9002/api/ldj/push', headers = {'Content-Type': 'application/json'})
        d['cid'] = ['3d0451a77ec82de9b8e9f32485299ae6']
        data = json.dumps(d, ensure_ascii = False)
        print('list getui: ', data)
        print urllib2.urlopen(req, data).read()

        req = urllib2.Request('http://127.0.0.1:9002/api/ldj/push', headers = {'Content-Type': 'application/json'})
        d['platform'] = 'sms'
        d['content'] = '您的验证码为8093，为了保护您的账户安全，验证码请勿转发他人【居品汇】'
        d['phonelist'] = ['18616149270']
        data = json.dumps(d, ensure_ascii = False)
        print('sms: ', data)
        print urllib2.urlopen(req, data).read()
