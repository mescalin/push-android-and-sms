#!/usr/bin/env python
#coding=utf-8

import flask
import log
import push

application = flask.Flask(__name__)

@application.route("/api/ldj/push", methods = ['POST'])
def push_notification():
    if flask.request.method == 'POST':
        return push.pushMessage(flask.request.data)

if __name__ == '__main__':
    application.config['DEBUG'] = True
    application.run(host = '0.0.0.0', port = 9002)
