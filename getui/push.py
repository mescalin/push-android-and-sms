#!/usr/bin/env python
#coding=utf-8

from igt_push import *
from igetui.template import *
from igetui.template.igt_base_template import *
from igetui.template.igt_transmission_template import *
from igetui.template.igt_link_template import *
from igetui.template.igt_notification_template import *
from igetui.template.igt_notypopload_template import *
from igetui.template.igt_apn_template import *
from igetui.igt_message import *
from igetui.igt_target import *
from igetui.template import *

import json
from common import *

_CONFIG = {
    'test': {
        APPID: 'ZmCrSoyCQf93DDHhOy3Bj3',
        APPKEY: 'drRPbiAF7m7YiJ0DoQ6eB1',
        MASTERSECRET: '0nVNJ7BHP56RWzFt5eLHN3',
        HOST: 'http://sdk.open.api.igexin.com/apiex.htm',
    },
    'com.ldjia.courier': {
        APPID: 'bxyq9znMLO6OjgntIReoe6',
        APPKEY: 'qKu4TS5L1v5LAVCwV2DLiA',
        MASTERSECRET: 'nsQ08B44D56bdNUGC9pWNA',
        HOST: 'http://sdk.open.api.igexin.com/apiex.htm'
    }
}

#可以推送给：所有用户，用户列表
#推送方式：通知消息（透传/下载/打开 url 链接）

#消息模版：
#1.TransmissionTemplate:透传功能模板
#2.LinkTemplate:通知打开链接功能模板
#3.NotificationTemplate：通知透传功能模板
#4.NotyPopLoadTemplate：通知弹框下载功能模板

def pushMessage(d):
    app = _CONFIG[d[APPNAME]]

    template = NotificationTemplate()
    template.appId = app[APPID]
    template.appKey = app[APPKEY]
    template.transmissionType = 1 #收到消息是否立即启动应用：1为立即启动，2则广播等待客户端自启动

    #d[TRANSMISSION_CONTENT] 是 unicode 编码
    tc = json.dumps(d[TRANSMISSION_CONTENT], ensure_ascii = False).encode('utf-8')
    #+ 以下三个字段是否需要 unicode 编码? 目前是全部转化为 utf-8
    template.transmissionContent = tc #透传内容，不支持转义字符
    template.title = d[TITLE].encode('utf-8')
    template.text = d[TEXT].encode('utf-8')

    template.logo = d.get(LOGO) #通知的图标名称，包含后缀名（需要在客户端开发时嵌入），如“push.png”
    template.logoURL = d.get(LOGO_URL) or ''
    template.isRing = d[RING]
    template.isVibrate = d[VIBRATE]
    template.isClearable = True

    message = None
    targets = []
    toapp = (len(d[CID]) == 0)
    if toapp: #如果 cid 列表为空，则 push 到所有用户
        message = IGtAppMessage()
        message.appIdList.extend([app[APPID]])
    else:
        message = IGtListMessage()
        for elt in d[CID]:
            t = Target()
            t.appId = app[APPID]
            t.clientId = elt
            targets.append(t)

    message.data = template
    message.isOffline = True
    message.offlineExpireTime = 1000 * 3600 * 12
    message.pushNetWorkType = 0 #设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送

    gt = IGeTui(app[HOST], app[APPKEY], app[MASTERSECRET])
    ret = None
    if toapp:
        ret = gt.pushMessageToApp(message)
    else:
        ret = gt.pushMessageToList(gt.getContentId(message), targets)
    #+ 假设 ret 中 key/val 不包含中文
    return json.dumps(ret)


def pushMessageToApp(d):
    app = _CONFIG[d[APPNAME]]

    template = NotificationTemplate()
    template.appId = app[APPID]
    template.appKey = app[APPKEY]
    template.transmissionType = 1 #收到消息是否立即启动应用：1为立即启动，2则广播等待客户端自启动

    #+ 以下三个字段是否需要 unicode 编码
    template.transmissionContent = d.get(TRANSMISSION_CONTENT) or '' #透传内容，不支持转义字符
    template.title = d[TITLE]
    template.text = d[TEXT]

    template.logo = d.get(LOGO) #通知的图标名称，包含后缀名（需要在客户端开发时嵌入），如“push.png”
    template.logoURL = d.get(LOGO_URL) or ''
    template.isRing = True
    template.isVibrate = True
    template.isClearable = True

    message = IGtAppMessage()
    message.data = template
    message.isOffline = True
    message.offlineExpireTime = 1000 * 3600 * 12
    message.appIdList.extend([app[APPID]])
    message.pushNetWorkType = 0 #设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送

    push = IGeTui(app[HOST], app[APPKEY], app[MASTERSECRET])
    ret = push.pushMessageToApp(message,'toApp_123')

def pushMessageToSingle(d):
    app = _CONFIG[d[APPNAME]]

    #template = TransmissionTemplate()
    template = NotificationTemplate()
    template.transmissionType = 1
    template.appId = app[APPID]
    template.appKey = app[APPKEY]

    template.transmissionType = 1 #收到消息是否立即启动应用：1为立即启动，2则广播等待客户端自启
    template.transmissionContent = d.get(TRANSMISSION_CONTENT) or '' #透传内容，不支持转义字符
    template.title = d[TITLE]
    template.text = d[TEXT]

    #iOS 推送需要的PushInfo字段 前三项必填，后四项可以填空字符串
    #template.setPushInfo(actionLocKey, badge, message, sound, payload, locKey, locArgs, launchImage)
    #template.setPushInfo("",2,"","","","","","",1);

    message = IGtSingleMessage()
    message.isOffline = True
    message.offlineExpireTime = 1000 * 3600 * 12
    message.data = template
    message.pushNetWorkType = 2

    target = Target()
    target.appId = app[APPID]
    target.clientId = d[CID][0]

    push = IGeTui(app[HOST], app[APPKEY], app[MASTERSECRET])
    print push.pushMessageToSingle(message, target)


def pushMessageToList(d):
    app = _CONFIG[d[APPNAME]]

    #template = LinkTemplate()
    template = NotificationTemplate()
    template.appId = app[APPID]
    template.appKey = app[APPKEY]

    template.title = u"请填入通知标题"
    template.text = u"请填入通知内容"
    template.logo = ""
    template.url = "http://www.baidu.com"
    template.transmissionType = 1
    template.transmissionContent = d.get(TRANSMISSION_CONTENT) or '' #透传内容，不支持转义字符
    template.isRing = True
    template.isVibrate = True
    template.isClearable = True

    message = IGtListMessage()
    message.data = template
    message.isOffline = True
    message.offlineExpireTime = 1000 * 3600 * 12
    message.pushNetWorkType = 0

    targets = []
    for elt in d[CID]:
        t = Target()
        t.appId = app[APPID]
        t.clientId = elt
        targets.append(t)

    push = IGeTui(app[HOST], app[APPKEY], app[MASTERSECRET])
    contentId = push.getContentId(message, 'ToList_任务别名')
    print push.pushMessageToList(contentId, targets)