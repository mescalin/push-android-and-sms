#!/usr/bin/env python
#coding=utf-8

import multiprocessing

bind = '0.0.0.0:9002'
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = 'eventlet'
worker_connections = 900
daemon = True

accesslog = './access.log'
errorlog = './err.log'
loglevel = 'info'

pidfile = './pid'
