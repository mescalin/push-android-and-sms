### 模块依赖：
* gunicorn
* eventlet
* flask
* beautifulsoup4
* 个推 sdk，放在子目录 getui/ 下

### 启动方式：
* 生产环境：gunicorn -c guincore_config.py app
* 测试：python -m app

### 配置：
* 端口等配置放在 guincore_config.py 中。如果以测试方式启动，端口写死在 app.py 中
* sms 的 账号 id，密码，后端 url 放在 common.py 中
* android 的 appid, apptoken 等配置放在 getui/push.py 中

###http api:
* url: http://&lt;host>/api/ldj/push
* method: post
* http body: json

<pre><code>
请求/应答中所有字符串都使用 utf-8 编码

platform 的值为以下三种:
    android
    sms
    ios

发送到 android:
{
    "platform": "android",
    "env": "sandbox/production",
    "appname": "",
    "cid": ["", "",], # clientid 列表, (len(cid) == 0:  push to app, len(cid) != 0: push to list)
    "title": "", #通知标题
    "text": "",  #通知内容
    "logo": "", #通知 logo
    "trans_content": { #发送给客户端: 转化为 json string
    },
    "ring": true/false, #是否响铃
    "vibrate": true/false, #是否振动
}

发送到 sms:
{
    "platform": "sms",
    "phonelist": ["", ""], #用户手机号码列表
    "content": '', #短信内容
}

reply:
{
    "result": "ok/...", # ok 代表成功, result 是其他字符串代表失败
    "": "",
}
</code></pre>
