#!/bin/bash

cur_date=`date "+%Y-%m-%d"`

cp push.log push.log.$cur_date
cp access.log access.log.$cur_date
cp err.log err.log.$cur_date

rm push.log access.log err.log

kill -USR1 `cat pid`
