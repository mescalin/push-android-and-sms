#!/usr/bin/env python
#coding=utf-8

import os, sys
sys.path.append(os.getcwd() + '/getui')

from bs4 import BeautifulSoup
import hashlib
import json
import urllib
import urllib2

from common import *
import log
import getui.push

def _pushSMS(d):
    data = {
        UID: SMS_UID,
        PWD: hashlib.md5(SMS_PWD).hexdigest(),
        CONTENT: d[CONTENT].encode('utf-8'),
        PHONELIST: ','.join(d[PHONELIST])
    }
    req = urllib2.urlopen(SMS_URL + '?' + urllib.urlencode(data))
    soup = BeautifulSoup(req.read())
    if soup.code.string == '0':
        return json.dumps({RESULT: 'ok'})
    else:
        return json.dumps({
            RESULT: soup.code.string.encode('utf-8'),
            DESCRIPTION: soup.description.string.encode('utf-8')}, ensure_ascii = False)

def pushMessage(data):
    ret = None
    try:
        d = json.loads(data) # d 是 unicode
        if d[PLATFORM] == ANDROID:
            ret =  getui.push.pushMessage(d)
        elif d[PLATFORM] == SMS:
            ret = _pushSMS(d)
        else:
            ret =  json.dumps({RESULT: 'not implenment'})
    except Exception as e:
        ret =  json.dumps({RESULT: e.message})
    log.logger.info('push:{}, ret:{}'.format(data, ret)) #统计流量
    return ret
